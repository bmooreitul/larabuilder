# iTul Laravel Builder Class

[![Latest Version on Packagist](https://img.shields.io/packagist/v/itul/larabuilder.svg?style=flat-square)](https://packagist.org/packages/itul/larabuilder)
[![Total Downloads](https://img.shields.io/packagist/dt/itul/larabuilder.svg?style=flat-square)](https://packagist.org/packages/itul/larabuilder)

The iTul Builder Class for laravel. This automates the same routes/traits/classes that are usually installed for debugging a project in laravel.

## Installation

You can install the package via composer:

**`composer require itul/larabuilder`**

## Usage

Once the package is installed into a laravel project, the routes and controller are automatically built. 

The controller will be placed in **`app/Http/Controllers/BuilderController.php`**
 
If the **`$_allowedUsers`** array has any email addresses in it, only the users with those emails will be allowed to execute a builder method.
 
The builder controller can auto route any public method name from a URI.

### Routing Examples

Within the builder controller any method that is added will automatically route by it's name.

| URL | Method |
| --- | --- |
| example.com/builder/test | `public function test(...)` |
| example.com/builder/complex-name | `public function complex_name(...)` |


## Credits

-   [Brandon Moore](https://https://bitbucket.org/bmooreitul/)

