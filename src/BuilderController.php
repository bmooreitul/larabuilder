<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class BuilderController extends Controller{

	use \Itul\Larabuilder\BuilderRouting;

	//IF $_requireAuth IS TRUE THEN A USER WILL HAVE TO BE LOGGED IN
	//IF $_requireAuth IS FALSE THEN ANY PUBLIC METHOD THIS CLASS HAS IS OPEN TO THE PUBLIC
	public $_requireAuth = true;

	//LIST USERS ALLOWED TO ACCESS THIS CLASS (IGNORED IF $_requireAuth = false)
	//IF THE $_allowedUsers ARRAY IS EMPTY ANY USER WILL BE ALLOWED TO ACCESS
	public $_allowedUsers = [
		//'user1@example.com',
		//'user2@example.com',
	];

	//WHITELISTED IP ADDRESSES. THIS WILL OVERRIDE AUTHENTICATION REQUIREMENTS IF THEY ARE ENABLED
	public $_allowedIps = [
		//'127.0.0.1',
		//'555.555.55.55',
	];

	public function test(Request $request){
		die('test builder route called');
	}
}