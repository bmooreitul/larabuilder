<?php

namespace Itul\Larabuilder;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Itul\Larabuilder\Skeleton\SkeletonClass
 */
class LarabuilderFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'larabuilder';
    }
}
