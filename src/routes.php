<?php

use Illuminate\Support\Facades\Route;

Route::prefix('builder')->middleware(['web'])->group(function () {
    Route::any('{any?}', 'App\Http\Controllers\BuilderController@route')->where('any', '.*');
});
