<?php

namespace Itul\Larabuilder;

use Illuminate\Support\ServiceProvider;

class LarabuilderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        /*
         * Optional methods to load your package assets
         */
        // $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'larabuilder');
        // $this->loadViewsFrom(__DIR__.'/../resources/views', 'larabuilder');
        // $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes.php');

        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/config.php' => config_path('larabuilder.php'),
            ], 'config');

            if(!file_exists(app_path('Http/Controllers/BuilderController.php'))){
                $this->publishes([
                    __DIR__.'/BuilderController.php' => app_path('Http/Controllers/BuilderController.php'),
                ], 'laravel-assets');
            }

            // Publishing the views.
            /*$this->publishes([
                __DIR__.'/../resources/views' => resource_path('views/vendor/larabuilder'),
            ], 'views');*/

            // Publishing assets.
            /*$this->publishes([
                __DIR__.'/../resources/assets' => public_path('vendor/larabuilder'),
            ], 'assets');*/

            // Publishing the translation files.
            /*$this->publishes([
                __DIR__.'/../resources/lang' => resource_path('lang/vendor/larabuilder'),
            ], 'lang');*/

            // Registering package commands.
            // $this->commands([]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        // Automatically apply the package configuration
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'larabuilder');

        // Register the main class to use with the facade
        $this->app->singleton('larabuilder', function () {
            return new Larabuilder;
        });
    }
}
