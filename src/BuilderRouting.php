<?php

namespace Itul\Larabuilder;
use Illuminate\Http\Request;

trait BuilderRouting{

    public function route(Request $request, $method = null, $parseSegments = true){

        if(is_string($method) && strpos($method, '/') === false) $parseSegments = false; 

        if(!isset($this->_requireAuth)) $this->_requireAuth = true;
        if(!isset($this->_allowedUsers)) $this->_allowedUsers = [];
        if(!isset($this->_allowedIps)) $this->_allowedIps = [];

        //DISABLE ROUTING FOR NON WHITELISTED IP ADDRESSES
        if(!empty($this->_allowedIps) && !in_array($request->ip(), $this->_allowedIps)) die('You do not have permission to view this page.');
        elseif(isset($this->_allowedIps) && !empty($this->_allowedIps) && in_array($request->ip(), $this->_allowedIps)) $this->_requireAuth = false;

        //DISABLE ROUTING FOR NON WHITELISTED USERS
        if($this->_requireAuth && (!auth()->user() || auth()->user() && !empty($this->_allowedUsers) && !in_array(auth()->user()->email, $this->_allowedUsers))) die('You do not have permission to view this page.');        

        //IF WE NEED TO PARSE THE REQUEST SEGMENTS
        if($parseSegments){

            //GET THE REVERSED SEGMENTS
            $reverseSegments = array_reverse($request->segments());

            //LOOP THROUGH THE REVERSED SEGMENTS
            foreach($reverseSegments as $segmentKey => $segment){

                //LOOP THROUGH THE CONTROLLERS
                foreach(array_reverse($this->_rglob(app_path('Http/Controllers'))) as $controllerPath){

                    //FORMAT THE TEMP CONTROLLER NAME
                    $tempControllerName = strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', pathinfo($controllerPath, PATHINFO_FILENAME)));

                    //STRIP THE CONTROLLER STRING FROM THE ROUTE
                    if(substr($tempControllerName, -11) == '-controller') $tempControllerName = substr($tempControllerName, 0, -11);

                    //SET THE TARGET SEGMENT
                    $targetSegment = strtolower(preg_replace('/(?<!^)[A-Z]/', '-$0', $segment));

                    //CHECK IF THE TEMP CONTROLLER NAME MATCHES THE TARGET SEGMENT
                    if($tempControllerName == $targetSegment){

                        //COMPILE THE FULLY QUALIFIED CONTROLLER NAME
                        $controllerName = '\\'.ltrim($this->_extract_namespace($controllerPath).'\\'.pathinfo($controllerPath, PATHINFO_FILENAME), '\\');

                        //REPARSE THE METHOD NAME
                        if(isset($reverseSegments[($segmentKey-1)])) $method = $reverseSegments[($segmentKey-1)];

                        //ROUTE TO THE SUB CONTROLLER
                        return (new $controllerName)->route($request, $method, false); //ROUTE WITHOUT PARSING THE SEGMENTS
                    }
                }
            }
        }

        //ROUTE THE METHOD IF NEEDED
        if($method) foreach(($class = new \ReflectionClass($this))->getMethods(\ReflectionMethod::IS_PUBLIC) as $m) if($m->class == $class->getName() && strtolower(str_replace('_', '-', $method)) == strtolower(str_replace('_', '-', $m->name))) return $this->{$m->name}($request);

        //NO METHOD TO ROUTE SO SHOW PHPINFO
        phpinfo();
        exit;
    }

    private function _rglob($dir, $flags=null, &$results=[]) {
        $ls = glob($dir, $flags);
        if(is_array($ls)) foreach($ls as $item) {
            if(is_dir($item)) $this->_rglob($item . '/*', $flags, $results);
            if(is_file($item)) $results[] = $item;
        }
        return $results;
    }

    private function _extract_namespace($file) {
        $ns     = NULL;
        $handle = fopen($file, "r");
        if($handle) {
            while(($line = fgets($handle)) !== false){
                if(strpos(trim($line), 'namespace') === 0){
                    $parts  = explode(' ', $line);
                    $ns     = rtrim(trim($parts[1]), ';');
                    break;
                }
            }
            fclose($handle);
        }
        return $ns;
    }
}